import datetime
import string
from collections import defaultdict

class Developer(object):

    def __init__(self, user, last_updated=None):
        print user
        self.user = user
        self.r = []
        self.last_updated = last_updated

    def incomplete(self):
        return not self.last_updated

    def username(self):
        return self.user['login']

    def url(self):
        return self.user['html_url']

    def name(self):
        try:
            return self.user['name']
        except KeyError:
            return self.username()

    def firstname(self):
        if not self.name():
            return ""
        return self.name().split()[0]

    def unique_languages(self):
        return set([r.language() for r in self.repos()])

    def picture(self):
        return self.user['avatar_url']

    def followers(self):
        return self.user['followers']

    def add_repo(self, repo):
        self.r.append(repo)

    def set_repos(self, repos):
        self.r = [Repo(r) for r in repos]

    def repos(self):
        return self.r

    def repos_by_importance(self):
        return sorted(self.repos(),
            key=lambda r: r.importance_score(self.username()),
            reverse=True)

    def to_json(self):
        return self.user

class Repo(object):

    def __init__(self, repo):
        self.repo = repo

    def importance_score(self, username):
        if (self.is_fork()):
            return 0
        importance = self.followers()
        importance += self.forks()
        importance += self.number_of_commits_by_user(username) / 10.0
        importance /= max(0.1, min(self.days_since_last_update() / 365.0, 1))
        return importance

    def url(self):
        return self.repo['html_url']

    def language(self):
        return self.repo['language']

    def name(self):
        return self.repo['name']

    def description(self):
        return self.repo['description']

    def is_fork(self):
        return self.repo['fork']

    def followers(self):
        return int(self.repo['watchers']) - 1

    def forks(self):
        return max(int(self.repo['forks']) - 1, 0)

    def set_commits(self, commits):
        self.repo['commits'] = commits

    def commits(self):
        return self.repo['commits']

    def set_comments(self, comments):
        self.repo['comments'] = comments

    def comments(self):
        return self.repo.get('comments', [])

    def number_of_commits(self):
        """Returns the total number of commits in this repo for all users."""
        return len(self.repo['commits'])

    def number_of_commits_by_user(self, username):
        """Returns the number of commits the given user had on this repo."""
        return len(self.commits_by_user(username))

    def commits_by_user(self, username):
        return filter(lambda c: self._is_committer(c, username),
            self.repo['commits'])

    def commit_tag_cloud(self, username):
        counts = defaultdict(int)
        for commit in self.commits_by_user(username):
            for word, count in self.word_counts(commit).iteritems():
                counts[word] += count
        return counts

    def word_counts(self, commit):
        counts = defaultdict(int)
        for k in commit['commit']['message'].split():
            counts[k.lower().strip(string.punctuation)] += 1
        return counts

    def _is_committer(self, commit, username):
        """Returns true if the given username is the author of the commit."""
        try:
            return commit['author']['login'] == username
        except:
            return False

    def days_since_last_update(self):
        delta = datetime.date.today() - self.last_updated()
        return delta.days

    def last_updated(self):
        try:
            commit_date = self.repo['commits'][-1]['commit']['committer']['date']
            datestr, timestr = commit_date.split('T')
            y, m, d = datestr.split('-')
            return datetime.date(int(y), int(m), int(d))
        except KeyError:
            return datetime.date(2010, 1, 1)

    def set_readme(self, readme):
        self.repo['readme'] = readme

    def readme(self):
        if self.repo['readme']:
            return self.repo['readme']['text']
        return None

    def to_json(self):
        return self.repo
