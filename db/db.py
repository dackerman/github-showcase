import riak
import model

_client = riak.RiakClient()
_user = _client.bucket('user')

def get_user(username):
	raw_data = _user.get(username).get_data()
	if not raw_data:
		return None

	dev = model.Developer(raw_data['user'])
	dev.set_repos(raw_data['repos'])
	return dev

def save_user(user):
	raw_data = {
		'user': user.to_json(),
		'repos': [r.to_json() for r in user.repos()]
	}
	newuser = _user.new(user.username(), data=raw_data)
	newuser.store()
	return newuser