import datetime
import json
import model
import pymongo

_connection = pymongo.Connection('localhost', 27017)
_db  = _connection.gitshowcase
_user = _db.users

def get_user(username):
	raw_data = _user.find_one({'_id': username})
	if not raw_data:
		return None

	return to_dev(raw_data)

def get_users_not_updated_since(since):
	old_data_or_no_last_updated_field = _user.find(
		{'$or':
			[{'last_updated': {'$exists': False}},
			{'last_updated': {'$lt': since}}]
		})

	return [to_dev(d) for d in old_data_or_no_last_updated_field]

def to_dev(raw_data):
	dev = model.Developer(raw_data['user'], raw_data.get('last_updated'))
	dev.set_repos(raw_data['repos'])
	return dev

def save_user(user, needs_update=False):
	db_user = {
		'_id': user.username(),
		'user': user.to_json(),
		'repos': [r.to_json() for r in user.repos()]
	}

	if not needs_update:
		db_user['last_updated'] = datetime.datetime.now()

	_user.save(db_user)
	return db_user