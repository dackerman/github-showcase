import datetime
import logging
import time

import githubapi
import model
from db import mongodb as db


def update_users():
	"""Updates the github data of users in the database that need it."""
	number_of_users_updated = 0
	print "Getting users to update..."

	yesterday = datetime.datetime.today() - datetime.timedelta(1)
	for user in db.get_users_not_updated_since(yesterday):
		print "Getting info for %s" % user.username()
		github_user = githubapi.get_developer(user.username())
		print "Saving info for %s" % user.username()
		save_user(github_user)
		number_of_users_updated += 1
		if githubapi.api_calls_left < 1000:
			print "Getting close to GitHub's API limit. Quitting for now."
			break

	print "Done. Updated %d users this run." % number_of_users_updated


def save_user(github_user):
	if github_user:
		db.save_user(github_user)
	else:
		print "Couldn't find %s on github... skipping" % user.username


if __name__ == "__main__":
	while True:
		update_users()
		time.sleep(5)