import githubapi
import model
from db import mongodb as db

import pprint

from flask import Flask
from flask import render_template
from flask import request
from flask import abort

app = Flask(__name__)

@app.route("/")
def main():
    return render_template('main.html')


@app.route("/<username>")
def showcase(username=None):
    user = None
    if should_check_cache(request):
        user = get_user_from_cache(username)

    if not user:
        print "Getting %s from github api..." % username
        user = githubapi.get_developer_summary(username)
        if not user:
            abort(404)
        cache_user(user)

    return render_template('showcase.html', user=user)


def should_check_cache(request):
    """Returns true if the user hasn't overridden
    the caching for this request."""
    return request.args.get('nocache', None) is None


def get_user_from_cache(username):
    """Pulls the user's information from the database."""
    cached_user = db.get_user(username)
    if (cached_user):
        print "Found %s data in cache" % username
        return cached_user
    else:
        print "Didn't find %s in cache" % username
        return None


def cache_user(user):
    """Stores the user's information in the database"""
    db.save_user(user, needs_update=True)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)