
var showcase = function(context) {
	var out = {}
	var WIDTH = 850;

	var slideshowContainer = function() {
		return context.document.getElementById('slideshow');
	};

	out.slideTo = function(index) {
		slideshowContainer().style.left = -(WIDTH * (index - 1));
		context.window.scrollTo(0, 0);
	};

	return out;
}(this);