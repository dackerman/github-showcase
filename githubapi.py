import base64
import httplib2
import json
import model


def for_user(username):
    return _GithubApi(username)


def get_developer_summary(username):
    """Gets the user's basic information from Github."""
    api = for_user(username)

    user_info = api.user_info()
    if not user_info:
        return None

    dev = model.Developer(user_info, last_updated=None)
    return dev


def get_developer(username):
    """Gets the user and repo information from the Github API."""
    api = for_user(username)

    user_info = api.user_info()
    if not user_info:
        return None

    dev = model.Developer(user_info)
    for api_repo in api.repos():
        repo = model.Repo(api_repo)
        repo.set_readme(api.readme(repo.name()))
        repo.set_commits(api.commits(repo.name()))
        repo.set_comments(api.comments(repo.name()))
        dev.add_repo(repo)

    return dev

api_calls_left = 5000 # may not be accurate - updated on first API call

class _GithubApi(object):

    def __init__(self, username):
        self.username = username
        self.h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)

    def user_info(self):
        return self._get_json("users")

    def repos(self):
        repos = self._get_json("users", "repos")
        print "%s has %d public repos" % (self.username, len(repos))
        return repos

    def readme(self, reponame):
        readme = self._get_json("repos", '%s/readme' % reponame)
        try:
            d = {
                'name': readme['name'],
                'text': unicode(self._parse_base64(readme['content']), 'utf8')
            }
            s = json.dumps(d)
            return d
        except KeyError:
            return ''
        except UnicodeDecodeError:
            return ''

    def _parse_base64(self, contents):
        return base64.b64decode(contents)

    def commits(self, reponame):
        return self._get_json("repos","%s/commits" % reponame)

    def comments(self, reponame):
        return self._get_json("repos", "%s/comments" % reponame)

    def _get_json(self, endpoint, params=None):
        url = _base_url(endpoint, self.username)
        if params:
            url += "/" + params
        try:
            string = self._get(url + "?per_page=100")
            s = json.dumps(json.loads(string), indent=4)
            return json.loads(string)
        except ValueError:
            return {}
        except UnicodeDecodeError:
            return {}

    def _get_raw(self, repo, filename):
        return self._get(_raw_url(self.username, repo, filename))

    def _get(self, url):
        resp, content = self.h.request(url, "GET")
        print "requesting %s (%s)" % (url, resp.status)
        global api_calls_left
        api_calls_left = int(resp['x-ratelimit-remaining'])
        print "API calls left: %d" % api_calls_left
        if resp.status == '404' or resp.status == 404:
            return ''
        return content

def _base_url(endpoint, username):
        return "https://api.github.com/{endpoint}/{user}".format(
            user=username, endpoint=endpoint)

def _raw_url(username, repo, filename):
        params = {'user': username, 'repo': repo, 'filename': filename}
        return "https://raw.github.com/{user}/{repo}/master/{filename}".format(**params)